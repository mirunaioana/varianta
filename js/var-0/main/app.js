function bowdlerize(input, dictionary){
    if (typeof input != "string") {
        throw new Error("Input should be a string");
    }
    var newString;
    for (let i = 0; i < dictionary.length; i++) {
        if (typeof dictionary[i] != "string") {
            throw new Error("Invalid dictionary format");
        }
        if (input.includes(dictionary[i])) {
            var newWord = dictionary[i].split("")
            for (let i = 0; i < newWord.length; i++) {
                if (i != 0 && i != newWord.length - 1) {
                    newWord[i] = "*"
                }
            }
            newWord = newWord.join("");
            input.replace(dictionary[i], newWord);
        }
    }
    return newString;
}

const app = {
    bowdlerize
};

module.exports = app;